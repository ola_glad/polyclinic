package com.geekhub.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Repository
public class ReportRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ReportRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<ReceptionReport> getReceptionReport(int doctorId, LocalDate startDate, LocalDate endDate) {
        String sql = "SELECT " +
                " r.doctor_id, d.first_name, d.last_name, " +
                " p.first_name as patient_first_name, p.last_name as patient_last_name, r.patient_id, " +
                " r.date, r.medication, r.status, " +
                " count(r.id) OVER (PARTITION BY d.id) as count_by_doctor, " +
                " count(r.id) OVER (PARTITION BY d.id, r.status) as count_by_status " +
            " FROM users d INNER JOIN receptions r ON d.id = r.doctor_id" +
            " INNER JOIN users p ON p.id = r.patient_id " +
            " WHERE r.date BETWEEN :startDate AND :endDate " +
                " AND r.doctor_id = :doctorId " +
            " ORDER BY d.first_name, d.last_name, r.date, r.status DESC";

        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("startDate", Date.valueOf(startDate))
                .addValue("endDate", Date.valueOf(endDate))
                .addValue("doctorId", doctorId);

        return jdbcTemplate.query(sql, parameters, (rs, cnt) -> {
            ReceptionReport report = new ReceptionReport();
            report.setDoctorId(rs.getInt("doctor_id"));
            report.setPatientId(rs.getInt("patient_id"));
            report.setDoctorFirstName(rs.getString("first_name"));
            report.setDoctorLastName(rs.getString("last_name"));
            report.setPatientFirstName(rs.getString("patient_first_name"));
            report.setPatientLastName(rs.getString("patient_last_name"));
            report.setDate(rs.getTimestamp("date").toLocalDateTime());
            report.setStatus(rs.getString("status"));
            report.setMedication(rs.getString("medication"));
            report.setCountReceptions(rs.getInt("count_by_doctor"));
            report.setCountByStatus(rs.getInt("count_by_status"));
            return report;
        });
    }

    public List<DoctorReport> getDoctorReport(LocalDate startDate, LocalDate endDate, String observableStatus) {
        String sql = "SELECT " +
                " t.id, t.first_name, t.last_name, t.cnt as total_count, t.status_count, " +
                " max(t.cnt) OVER () AS max_count, min(t.cnt) OVER () AS min_count " +
            " FROM ( " +
                " SELECT " +
                " d.first_name, d.last_name, d.id, count(r.*) AS cnt, " +
                " count(CASE WHEN r.status = :observableStatus THEN r.id END) as status_count " +
                " FROM users d INNER JOIN receptions r ON d.id = r.doctor_id " +
                " WHERE r.date BETWEEN :startDate AND :endDate " +
                " GROUP BY d.id) t " +
            " ORDER BY t.cnt DESC, t.first_name, t.last_name";

        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("startDate", Date.valueOf(startDate))
                .addValue("endDate", Date.valueOf(endDate))
                .addValue("observableStatus", observableStatus);

        return jdbcTemplate.query(sql, parameters, (rs, cnt) -> {
            DoctorReport report = new DoctorReport();
            report.setDoctorId(rs.getInt("id"));
            report.setFirstName(rs.getString("first_name"));
            report.setLastName(rs.getString("last_name"));
            report.setTotalCount(rs.getInt("total_count"));
            report.setStatusCount(rs.getInt("status_count"));
            report.setMaxCount(rs.getInt("max_count"));
            report.setMinCount(rs.getInt("min_count"));
            return report;
        });
    }
}

