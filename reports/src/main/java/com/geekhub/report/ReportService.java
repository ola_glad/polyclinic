package com.geekhub.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ReportService {
    private final ReportRepository reportRepository;

    @Autowired
    public ReportService(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    public List<ReceptionReport> getReceptionReport(int doctorId, LocalDate startDate, LocalDate endDate) {
        return reportRepository.getReceptionReport(doctorId, startDate, endDate);
    }

    public List<DoctorReport> getDoctorReport(LocalDate startDate, LocalDate endDate, String status) {
        return reportRepository.getDoctorReport(startDate, endDate, status);
    }
}
