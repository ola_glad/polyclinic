const handleErrors = (startDate, endDate, data) => {
    if (data.errors) {
        const errors = data.errors.reduce((errorData, {field, defaultMessage}) => {
            return {...errorData, [field]: defaultMessage};
        }, {});
        if (errors.startDate) {
            startDate.addClass('is-invalid');
            startDate.parent().find('.invalid-feedback').text(errors.startDate);
        }
        if (errors.endDate) {
            endDate.addClass('is-invalid');
            endDate.parent().find('.invalid-feedback').text(errors.endDate);
        }
    }
};
