$(function() {
    const getTime = (schedules, cellDate) =>
        (schedules || []).find(({date}) => dateFromLocalDate(date).getTime() === cellDate.getTime());

    const buttons = (doctorId, date, timeData) => {
        const strDate = moment(date).format('YYYY-MM-DD');
        const addButton = timeData ? '' : `<a class="btn btn-primary btn-sm" role="button" href="/schedule/add/${doctorId}/${strDate}">+</a>`;
        const editButton = timeData ? `<a class="btn btn-primary btn-sm" role="button" href="/schedule/edit/${doctorId}/${strDate}">^</a>` : '';
        const deleteButton = timeData ? `<form action="/schedule/delete/${doctorId}/${strDate}" method="post">
                <button type="submit" class="btn btn-primary btn-sm">-</button>
            </form>` : '';
        return $(`<span class="buttons">${addButton}&nbsp;${editButton}&nbsp;${deleteButton}</span>`);
    };

    const updateSchedule = () => {
        const table = $('.schedule__data').empty();

        tableSchedule.forEach(el => {
            const {doctor: {fullName, id} = {}, schedule, canEdit} = el;
            const row = $('<tr/>');
            $(`<td>${fullName}</td>`).appendTo(row);
            for (let d = new Date(startDate); d < endDate; d.setDate(d.getDate() + 1)) {
                const {startTime, endTime} = getTime(schedule, d) || {};
                const timeData = startTime && endTime ?
                    `${formatTime(dateFromLocalTime(startTime))} - ${formatTime(dateFromLocalTime(endTime))}` : '';
                const cell = $(`<td><div>${timeData}</div></td>`).appendTo(row);
                if (canEdit) {
                    cell.append(buttons(id, d, timeData));
                }
            }
            row.appendTo(table);
        });
    };

    let scheduleFull = [];
    let tableSchedule = [];
    const header = $('.schedule__header');
    const startDate = new Date();
    startDate.setHours(0, 0, 0, 0);
    const endDate = new Date(startDate);
    endDate.setMonth(endDate.getMonth() + 1);
    for (let d = new Date(startDate); d < endDate; d.setDate(d.getDate() + 1)) {
        $(`<th>${moment(d).format('DD.MM.YYYY')}</th>`).appendTo(header);
    }

    $('#doctors').change(({target}) => {
        const doctorId = +$(target).val();
        tableSchedule = doctorId === 0 ? scheduleFull : scheduleFull.filter(({doctor: {id}}) => id === doctorId);
        updateSchedule();
    });

    $.get('/schedule/data', (scheduleDto) => {
        scheduleFull = scheduleDto;
        tableSchedule = scheduleDto;
        updateSchedule();
    })
});