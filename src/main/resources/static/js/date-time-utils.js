const dateFromLocalDate = ({dayOfMonth, monthValue, year}) => new Date(year, monthValue - 1, dayOfMonth);

const dateFromLocalTime = ({hour, minute} = {hour: 0, minute: 0}) => {
    const date = new Date();
    date.setHours(hour, minute);
    return date;
};

const dateFromLocalDateTime = ({dayOfMonth, monthValue, year, hour, minute}) =>
    new Date(year, monthValue - 1, dayOfMonth, hour, minute);

const formatTime = time => moment(time).format('HH:mm');
