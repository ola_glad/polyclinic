$(function() {
    const updateTable = (data) => {
        startDate.removeClass('is-invalid');
        endDate.removeClass('is-invalid');
        tableReport.show();
        const table = $('.doctors').empty();
        data.forEach(el => {
            const {doctor, totalCount, statusCount, maxCount, minCount} = el;
            const row = $('<tr/>');
            $('<td>' + doctor + '</td>').appendTo(row);
            $('<td>' + totalCount + '</td>').appendTo(row);
            $('<td>' + statusCount + '</td>').appendTo(row);
            $('<td>' + maxCount + '</td>').appendTo(row);
            $('<td>' + minCount + '</td>').appendTo(row);
            row.appendTo(table);
        });
    };
    const startDate = $('#startDate');
    const endDate = $('#endDate');
    const tableReport = $('.doctors-report').hide();
    $('#btn-run').click(() => {
        $.post("/statistic/doctors", {
            startDate: startDate.val(),
            endDate: endDate.val()
        }, updateTable).fail(({responseJSON}) => handleErrors(startDate, endDate, responseJSON));
    });
});