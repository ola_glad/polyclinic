$(function() {
    const updateTable = (data) => {
        startDate.removeClass('is-invalid');
        endDate.removeClass('is-invalid');
        tableReport.show();
        const table = $('.receptions').empty();
        data.forEach(el => {
            const {doctor, patient, date, medication, status, countReceptions, countByStatus} = el;
            const row = $('<tr/>');
            $('<td>' + doctor + '</td>').appendTo(row);
            $('<td>' + patient + '</td>').appendTo(row);
            $('<td>' + moment(dateFromLocalDateTime(date)).format('DD.MM.YYYY HH:mm') + '</td>').appendTo(row);
            $('<td>' + (medication || '') + '</td>').appendTo(row);
            $('<td>' + status + '</td>').appendTo(row);
            $('<td>' + countReceptions + '</td>').appendTo(row);
            $('<td>' + countByStatus + '</td>').appendTo(row);
            row.appendTo(table);
        });
    };
    const doctors = $('#doctors');
    const startDate = $('#startDate');
    const endDate = $('#endDate');
    const tableReport = $('.receptions-report').hide();
    $('#btn-run').click(() => {
        $.post("/statistic/receptions", {
            doctorId: doctors.val(),
            startDate: startDate.val(),
            endDate: endDate.val()
        }, updateTable).fail(({responseJSON}) => handleErrors(startDate, endDate, responseJSON));
    });
});