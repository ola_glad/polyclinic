$(function() {
    const roles = {
        ADMIN: 'ADMIN',
        DOCTOR: 'DOCTOR',
        PATIENT: 'PATIENT'
    };

    const changePage = (page) => $.get(`/admin/users?page=${page}`, updatePage);

    const updatePage = ({data, totalPages, pagination: {page}}) => {
        const table = $('.admin-users').empty();
        data.forEach(el => {
            const {id, fullName, login, email, phone, role, active} = el;
            const row = $('<tr/>');
            $('<td>' + login + '</td>').appendTo(row);
            $('<td>' + fullName + '</td>').appendTo(row);
            $('<td>' + email + '</td>').appendTo(row);
            $('<td>' + phone + '</td>').appendTo(row);
            $('<td>' + role + '</td>').appendTo(row);
            $('<td>' + active + '</td>').appendTo(row);
            if (role !== roles.PATIENT) {
                const editButton = `<a role="button" class="btn btn-info btn-sm" href="/admin/edit/${id}">Edit</a>`;
                const inactivateButton = active ?
                    `<form action="/admin/inactivate/${id}" method="post">
                        <button type="submit" class="btn btn-primary btn-sm">Inactivate</button>
                    </form>` : '';
                $(`<td class="buttons">${editButton}&nbsp;${inactivateButton}</td>`).appendTo(row);
            } else {
                $('<td/>').appendTo(row);
            }
            row.appendTo(table);
        });
        if (totalPages > 1) {
            const pagination = $('.pagination').empty();
            pagination.append($(`
                <li class="page-item ${page === 1 ? 'disabled' : ''}">
                    <button class="page-link" type="button">Prev</button>
                </li>`).click(() => changePage(page - 1)));
            for (let i = 1; i <= totalPages; i++) {
                $(`
                <li class="page-item ${page === i ? 'active' : ''}">
                    <button class="page-link" type="button">${i}</button>
                </li>`).click(() => changePage(i)).appendTo(pagination);
            }
            pagination.append($(`
                <li class="page-item ${page === totalPages ? 'disabled' : ''}">
                    <button class="page-link" type="button">Next</button>
                </li>`).click(() => changePage(page + 1)));
        }
    };

    $.get('/admin/users', updatePage);
});