$(function() {
    const RECEPTION_TIME_MIN = 30;
    const RECEPTION_DAYS_COUNT = 10;

    const onClickDate = (date) => {
        const strDate = moment(date).format('YYYY-MM-DD');
        dateInput.val(strDate);
        $.get(`/schedule/data/${doctor.val()}/${strDate}`, updateSchedule);
        $.get(`/reception/requested/${doctor.val()}/${strDate}`, setBusyDisabled);
    };

    const onClickTime = (time) => {
        timeInput.val(formatTime(time));
        submitButton.prop('disabled', false);
    };

    const appendDateButton = (date, row) => {
        $(`<td></td>`).appendTo(row).append(
            $(`<button type="button" class="btn btn-secondary btn-date">${moment(date).format('DD.MM.YYYY')}</button>`)
                .click(() => onClickDate(date))
        );
    };

    const appendTimeButton = (date, row) => {
        $(`<td></td>`).appendTo(row).append(
            $(`<button type="button" class="btn btn-info btn-time">${formatTime(date)}</button>`)
                .click(() => onClickTime(date))
        );
    };

    const setBusyDisabled = (request) => {
        const timeButtons = $(".btn-time");
        request.forEach(({date}) => {
            const requestDate = dateFromLocalDateTime(date);
            timeButtons.filter(function() {
                return $(this).text() === formatTime(requestDate);
            }).prop('disabled', true);
        });
    };

    const updateSchedule = ({startTime, endTime}) => {
        if (startTime && endTime) {
            const start = dateFromLocalTime(startTime);
            const end = dateFromLocalTime(endTime);
            const tableTime = $('.reception-time').empty();
            let row;
            let index = 0;
            for (let d = new Date(start); d < end; d.setMinutes(d.getMinutes() + RECEPTION_TIME_MIN), index++) {
                if (index % 5 === 0) {
                    row = $('<tr/>').appendTo(tableTime);
                }
                appendTimeButton(new Date(d), row);
            }
        }
    };

    const tableDate = $('.reception-date');
    const dateInput = $('#date');
    const timeInput = $('#time');
    const submitButton = $('input[type="submit"]');
    const dateButtons = $('.btn-date');
    const doctor = $('#doctorId').change(() => {
        dateButtons.removeClass('active');
        dateInput.val('');
        timeInput.val('');
        submitButton.prop('disabled', true);
    });

    const startDate = new Date();
    startDate.setHours(0, 0, 0, 0);
    const endDate = new Date(startDate);
    endDate.setDate(endDate.getDate() + RECEPTION_DAYS_COUNT);

    let row;
    let index = 0;
    for (let d = new Date(startDate); d < endDate; d.setDate(d.getDate() + 1), index++) {
        if (index % 2 === 0) {
            row = $('<tr/>').appendTo(tableDate);
        }
        appendDateButton(new Date(d), row);
    }
});