ALTER TABLE receptions DROP CONSTRAINT receptions_illness_id_fkey;
ALTER TABLE receptions DROP COLUMN illness_id;
DROP TABLE illness;