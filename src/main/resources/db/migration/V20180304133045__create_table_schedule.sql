CREATE TABLE schedule (
  id SERIAL PRIMARY KEY NOT NULL,
  doctor_id INT REFERENCES users(id) NOT NULL,
  date DATE,
  start_time TIME,
  end_time TIME
);