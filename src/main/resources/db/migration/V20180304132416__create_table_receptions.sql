CREATE TABLE receptions (
  id SERIAL PRIMARY KEY NOT NULL,
  doctor_id INT REFERENCES users(id) NOT NULL,
  patient_id INT REFERENCES users(id) NOT NULL,
  date TIMESTAMP NOT NULL,
  illness_id INT REFERENCES illness(id),
  medication CHARACTER VARYING(1000),
  status CHARACTER VARYING(20) NOT NULL
);