ALTER TABLE schedule ALTER COLUMN date SET NOT NULL;
ALTER TABLE schedule ALTER COLUMN start_time SET NOT NULL;
ALTER TABLE schedule ALTER COLUMN end_time SET NOT NULL;

ALTER TABLE schedule ADD CONSTRAINT check_time_order CHECK (start_time <= schedule.end_time);

CREATE INDEX ind_schedule ON schedule (doctor_id, date);
