CREATE TABLE patients (
  user_id INT REFERENCES users (id) NOT NULL,
  birthday DATE,
  address CHARACTER VARYING(200)
);