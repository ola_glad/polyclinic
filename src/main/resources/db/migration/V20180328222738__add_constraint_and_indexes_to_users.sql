ALTER TABLE users ADD CONSTRAINT check_min_length CHECK (length(login) >= 5);

CREATE INDEX ind_login ON users (login);
