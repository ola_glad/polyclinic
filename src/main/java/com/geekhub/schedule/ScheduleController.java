package com.geekhub.schedule;

import com.geekhub.doctor.DoctorException;
import com.geekhub.doctor.DoctorService;
import com.geekhub.user.CustomUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/schedule")
public class ScheduleController {
    private final ScheduleService scheduleService;
    private final DoctorService doctorService;

    @Autowired
    public ScheduleController(ScheduleService scheduleService, DoctorService doctorService) {
        this.scheduleService = scheduleService;
        this.doctorService = doctorService;
    }

    @GetMapping
    public String getSchedulePage(ModelMap model) {
        model.addAttribute("doctors", doctorService.getActiveDoctors());
        return "schedule";
    }

    @GetMapping("/data")
    @ResponseBody
    public List<ScheduleDto> getScheduleData(@AuthenticationPrincipal User user) {
        return user == null ? scheduleService.getMonthlySchedule ()
                : scheduleService.getMonthlySchedule(((CustomUser) user).getId());
    }

    @GetMapping("/data/{doctorId}/{date}")
    @ResponseBody
    public ScheduleEntryDto getScheduleData(
            @PathVariable int doctorId,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable LocalDate date
    ) {
        return scheduleService.getScheduleDto(doctorId, date);
    }


    @GetMapping("/add/{doctorId}/{date}")
    public String addSchedule(
            @PathVariable int doctorId,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable LocalDate date,
            ModelMap model
    ) throws DoctorException {
        model.addAttribute("schedule", scheduleService.getNewSchedule(doctorId, date));
        model.addAttribute("doctor", doctorService.getDoctorById(doctorId));
        return "schedule-edit";
    }

    @GetMapping("/edit/{doctorId}/{date}")
    public String editSchedule(
            @PathVariable int doctorId,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable LocalDate date,
            ModelMap model
    ) throws DoctorException {
        model.addAttribute("schedule", scheduleService.getSchedule(doctorId, date));
        model.addAttribute("doctor", doctorService.getDoctorById(doctorId));
        return "schedule-edit";
    }

    @PostMapping("/save")
    public String saveSchedule(@ModelAttribute("schedule") @Valid Schedule schedule, BindingResult bindingResult, ModelMap model) throws DoctorException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("doctor", doctorService.getDoctorById(schedule.getDoctorId()));
            return "schedule-edit";
        }
        scheduleService.save(schedule);
        return "redirect:/schedule";
    }

    @PostMapping("/delete/{doctorId}/{date}")
    public String deleteSchedule(
            @PathVariable int doctorId,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable LocalDate date
    ) {
        scheduleService.delete(doctorId, date);
        return "redirect:/schedule";
    }
}
