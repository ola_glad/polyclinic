package com.geekhub.schedule;

import com.geekhub.doctor.DoctorDto;

import java.util.List;

public class ScheduleDto {
    private DoctorDto doctor;
    private boolean canEdit;
    private List<ScheduleEntryDto> schedule;

    public DoctorDto getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorDto doctor) {
        this.doctor = doctor;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public List<ScheduleEntryDto> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<ScheduleEntryDto> schedule) {
        this.schedule = schedule;
    }
}
