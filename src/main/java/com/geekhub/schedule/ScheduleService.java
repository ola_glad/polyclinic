package com.geekhub.schedule;

import com.geekhub.doctor.DoctorDto;
import com.geekhub.doctor.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ScheduleService {
    private final DoctorService doctorService;
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public ScheduleService(DoctorService doctorService, ScheduleRepository scheduleRepository) {
        this.doctorService = doctorService;
        this.scheduleRepository = scheduleRepository;
    }

    public List<ScheduleDto> getMonthlySchedule() {
        Map<Integer, DoctorDto> activeDoctors = doctorService.getActiveDoctors().stream()
                .collect(Collectors.toMap(DoctorDto::getId, Function.identity(), (a, b) -> a, LinkedHashMap::new));
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusMonths(1);

        List<Schedule> schedules = scheduleRepository.getScheduleByDoctorsForPeriod(activeDoctors.keySet(), startDate, endDate);
        Map<Integer, List<ScheduleEntryDto>> scheduleByDoctor = schedules.stream()
                .collect(Collectors.groupingBy(Schedule::getDoctorId,
                        Collectors.mapping(this::convertScheduleToDto, Collectors.toList())));
        return activeDoctors.entrySet().stream().map(entry -> {
            ScheduleDto scheduleDto = new ScheduleDto();
            scheduleDto.setDoctor(entry.getValue());
            scheduleDto.setCanEdit(false);
            List<ScheduleEntryDto> doctorSchedule = scheduleByDoctor.get(entry.getKey());
            scheduleDto.setSchedule(doctorSchedule == null ? new ArrayList<>() : doctorSchedule);
            return scheduleDto;
        }).collect(Collectors.toList());
    }

    public List<ScheduleDto> getMonthlySchedule(int userId) {
        List<ScheduleDto> monthlySchedule = getMonthlySchedule();
        monthlySchedule.forEach(schedule -> schedule.setCanEdit(schedule.getDoctor().getId() == userId));
        return monthlySchedule;
    }

    public Schedule getSchedule(int doctorId, LocalDate date) {
        return scheduleRepository.getScheduleByDoctorOnDate(doctorId, date)
                .orElseGet(() -> getNewSchedule(doctorId, date));
    }

    public ScheduleEntryDto getScheduleDto(int doctorId, LocalDate date) {
        return convertScheduleToDto(getSchedule(doctorId, date));
    }

    public Schedule getNewSchedule(int doctorId, LocalDate date) {
        Schedule schedule = new Schedule();
        schedule.setDoctorId(doctorId);
        schedule.setDate(date);
        return schedule;
    }

    public void save(Schedule schedule) {
        scheduleRepository.saveSchedule(schedule);
    }

    public void delete(int doctorId, LocalDate date) {
        scheduleRepository.deleteSchedule(doctorId, date);
    }

    private ScheduleEntryDto convertScheduleToDto(Schedule schedule) {
        ScheduleEntryDto scheduleEntryDto = new ScheduleEntryDto();
        scheduleEntryDto.setDate(schedule.getDate());
        scheduleEntryDto.setStartTime(schedule.getStartTime());
        scheduleEntryDto.setEndTime(schedule.getEndTime());
        return scheduleEntryDto;
    }
}
