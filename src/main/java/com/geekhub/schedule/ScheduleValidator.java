package com.geekhub.schedule;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalTime;

import static com.geekhub.utils.ValidatorUtils.setErrorOnField;

public class ScheduleValidator implements ConstraintValidator<ScheduleConstraint, Schedule> {
    @Override
    public void initialize(ScheduleConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(Schedule value, ConstraintValidatorContext context) {
        LocalTime startTime = value.getStartTime();
        LocalTime endTime = value.getEndTime();

        if (startTime == null) {
            setErrorOnField("startTime", "{notNull}", context);
            return false;
        }

        if (endTime == null) {
            setErrorOnField("endTime", "{notNull}", context);
            return false;
        }

        if (startTime.isAfter(endTime)) {
            setErrorOnField("startTime", "{time.order}", context);
            return false;
        }

        return true;
    }
}
