package com.geekhub.schedule.download;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import static com.geekhub.schedule.download.DownloadType.*;

@Controller
@RequestMapping("/schedule/download")
public class DownloadController {
    private final DownloadService downloadService;
    private final ExcelScheduleDownloader excelDownloader;
    private final WordScheduleDownloader wordDownloader;
    private final PdfScheduleDownloader pdfDownloader;

    @Autowired
    public DownloadController(DownloadService downloadService, ExcelScheduleDownloader excelDownloader, WordScheduleDownloader wordDownloader, PdfScheduleDownloader pdfDownloader) {
        this.downloadService = downloadService;
        this.excelDownloader = excelDownloader;
        this.wordDownloader = wordDownloader;
        this.pdfDownloader = pdfDownloader;
    }

    @PostMapping("/excel")
    void downloadExcel(HttpServletResponse response) throws DownloadException {
        downloadService.downloadSchedule(XLSX, excelDownloader, response);
    }

    @PostMapping("/word")
    void downloadWord(HttpServletResponse response) throws DownloadException {
        downloadService.downloadSchedule(DOCX, wordDownloader, response);
    }

    @PostMapping("/pdf")
    void downloadPdf(HttpServletResponse response) throws DownloadException {
        downloadService.downloadSchedule(PDF, pdfDownloader, response);
    }
}
