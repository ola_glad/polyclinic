package com.geekhub.schedule.download;

import com.geekhub.schedule.ScheduleDto;
import com.geekhub.schedule.ScheduleEntryDto;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Component
public class ExcelScheduleDownloader implements ScheduleDownloader {
    private final DownloadUtils downloadUtils;

    @Autowired
    public ExcelScheduleDownloader(DownloadUtils downloadUtils) {
        this.downloadUtils = downloadUtils;
    }

    @Override
    public ByteArrayOutputStream getDownloadFile(List<ScheduleDto> schedule) throws DownloadException {
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusMonths(1);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Schedule");
        CellStyle headerStyle = getHeaderStyle(workbook);
        CellStyle cellStyle = getCellStyle(workbook);

        int rownum = 0;
        int colnum = 0;
        Row row = sheet.createRow(rownum++);
        createCell(row.createCell(0), downloadUtils.getHeaderData(startDate, endDate), getTitleStyle(workbook));
        row.setHeightInPoints(30);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, schedule.size()));

        row = sheet.createRow(rownum++);
        createCell(row.createCell(colnum++), "Data", headerStyle);

        for (ScheduleDto dto : schedule) {
            createCell(row.createCell(colnum++), dto.getDoctor().getFullName(), headerStyle);
        }

        for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
            LocalDate currentDate = date;
            row = sheet.createRow(rownum++);
            colnum = 0;
            createCell(row.createCell(colnum++), downloadUtils.getFormattedDate(currentDate), headerStyle);

            for (ScheduleDto dto : schedule) {
                List<ScheduleEntryDto> scheduleEntries = dto.getSchedule();

                Optional<String> formattedTime = scheduleEntries.stream()
                        .filter((time) -> time.getDate().equals(currentDate))
                        .map(scheduleEntry ->
                                downloadUtils.getFormattedTime(scheduleEntry.getStartTime(), scheduleEntry.getEndTime()))
                        .findAny();
                createCell(row.createCell(colnum++), formattedTime.orElse(""), cellStyle);
            }
        }
        Stream.iterate(0, (i) -> i + 1).limit(schedule.size() + 1).forEach(sheet::autoSizeColumn);

        try(ByteArrayOutputStream data = new ByteArrayOutputStream()) {
            workbook.write(data);
            return data;
        } catch (IOException e) {
            throw new DownloadException("Excel file can't be generated!", e);
        }
    }

    private void createCell(Cell cell, String value, CellStyle style) {
        cell.setCellValue(value);
        cell.setCellStyle(style);
    }

    private CellStyle getTitleStyle(Workbook workbook) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short)14);
        font.setBold(true);

        CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return style;
    }

    private CellStyle getHeaderStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setBorderTop(BorderStyle.MEDIUM);
        style.setBorderBottom(BorderStyle.MEDIUM);
        style.setBorderLeft(BorderStyle.MEDIUM);
        style.setBorderRight(BorderStyle.MEDIUM);
        return style;
    }

    private CellStyle getCellStyle(Workbook workbook) {
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        return style;
    }
}
