package com.geekhub.schedule.download;

import com.geekhub.schedule.ScheduleDto;
import com.geekhub.schedule.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Service
public class DownloadService {
    private final ScheduleService scheduleService;

    @Autowired
    public DownloadService(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    void downloadSchedule(DownloadType type, ScheduleDownloader downloader, HttpServletResponse response) throws DownloadException {
        List<ScheduleDto> schedule = scheduleService.getMonthlySchedule();
        try {
            downloader.getDownloadFile(schedule).writeTo(response.getOutputStream());
            response.setContentType(type.getContentType());
            response.setHeader("Content-disposition", "attachment; filename=schedule." + type.getExtension());
        } catch (IOException e) {
            throw new DownloadException("File can not download", e);
        }
    }
}
