package com.geekhub.schedule.download;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Component
public class DownloadUtils {
    private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public String getFormattedTime(LocalTime startTime, LocalTime endTime) {
        return startTime.format(timeFormatter) + " - " + endTime.format(timeFormatter);
    }

    public String getHeaderData(LocalDate startDate, LocalDate endDate) {
        String headerData = getFormattedDate(startDate) + " - " + getFormattedDate(endDate.minusDays(1));
        return "Doctors monthly schedule " + headerData;
    }

    public String getFormattedDate(LocalDate date) {
        return date.format(dateFormatter);
    }
}
