package com.geekhub.schedule.download;

import com.geekhub.schedule.ScheduleDto;
import com.geekhub.schedule.ScheduleEntryDto;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

@Component
public class WordScheduleDownloader implements ScheduleDownloader {
    private final DownloadUtils downloadUtils;
    private final BigInteger TABLE_WIDTH = BigInteger.valueOf(9000);

    @Autowired
    public WordScheduleDownloader(DownloadUtils downloadUtils) {
        this.downloadUtils = downloadUtils;
    }

    @Override
    public ByteArrayOutputStream getDownloadFile(List<ScheduleDto> schedule) throws DownloadException {
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusMonths(1);

        XWPFDocument document = new XWPFDocument();
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);

        XWPFRun documentTitle = paragraph.createRun();
        documentTitle.setFontSize(14);
        documentTitle.setBold(true);
        documentTitle.setText(downloadUtils.getHeaderData(startDate, endDate));

        XWPFTable table = document.createTable((int) DAYS.between(startDate, endDate) + 1, schedule.size() + 1);
        table.getCTTbl().addNewTblPr().addNewTblW().setW(TABLE_WIDTH);
        int rownum = 0;
        XWPFTableRow row = table.getRow(rownum++);
        int colnum = 0;
        createHeaderCell(row.getCell(colnum++), "Data");
        for (ScheduleDto dto : schedule) {
            createHeaderCell(row.getCell(colnum++), dto.getDoctor().getFullName());
        }

        for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
            LocalDate currentDate = date;
            row = table.getRow(rownum++);
            colnum = 0;
            createHeaderCell(row.getCell(colnum++), downloadUtils.getFormattedDate(currentDate));

            for (ScheduleDto dto : schedule) {
                List<ScheduleEntryDto> scheduleEntries = dto.getSchedule();

                Optional<String> formattedTime = scheduleEntries.stream()
                        .filter((time) -> time.getDate().equals(currentDate))
                        .map(scheduleEntry ->
                                downloadUtils.getFormattedTime(scheduleEntry.getStartTime(), scheduleEntry.getEndTime()))
                        .findAny();
                row.getCell(colnum++).setText(formattedTime.orElse(""));
            }
        }

        try(ByteArrayOutputStream data = new ByteArrayOutputStream()) {
            document.write(data);
            return data;
        } catch (IOException e) {
            throw new DownloadException("Word file can't be generated!", e);
        }
    }


    private void createHeaderCell(XWPFTableCell cell, String value) {
        cell.setText(value);
        cell.setColor("D3D3D3");
    }
}
