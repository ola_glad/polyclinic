package com.geekhub.schedule.download;

import com.geekhub.schedule.ScheduleDto;
import com.geekhub.schedule.ScheduleEntryDto;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class PdfScheduleDownloader implements ScheduleDownloader {
    private final DownloadUtils downloadUtils;

    @Autowired
    PdfScheduleDownloader(DownloadUtils downloadUtils) {
        this.downloadUtils = downloadUtils;
    }

    @Override
    public ByteArrayOutputStream getDownloadFile(List<ScheduleDto> schedule) throws DownloadException {
        try(ByteArrayOutputStream data = new ByteArrayOutputStream()) {
            LocalDate startDate = LocalDate.now();
            LocalDate endDate = startDate.plusMonths(1);

            Document document = new Document(PageSize.A4.rotate(), 10, 10, 10, 10);
            PdfWriter.getInstance(document, data);
            document.open();
            Paragraph headerTitle = new Paragraph(downloadUtils.getHeaderData(startDate, endDate));
            headerTitle.setAlignment(Element.ALIGN_CENTER);
            document.add(headerTitle);

            PdfPTable table = new PdfPTable(schedule.size() + 1);
            table.setWidthPercentage(100);
            table.setSpacingBefore(10);
            table.addCell(createHeader("Data"));
            schedule.forEach(item -> table.addCell(createHeader(item.getDoctor().getFullName())));

            for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
                LocalDate currentDate = date;
                table.addCell(createHeader(downloadUtils.getFormattedDate(currentDate)));

                for (ScheduleDto dto : schedule) {
                    List<ScheduleEntryDto> scheduleEntries = dto.getSchedule();

                    Optional<String> formattedTime = scheduleEntries.stream()
                            .filter((time) -> time.getDate().equals(currentDate))
                            .map(scheduleEntry ->
                                    downloadUtils.getFormattedTime(scheduleEntry.getStartTime(), scheduleEntry.getEndTime()))
                            .findAny();
                    table.addCell(formattedTime.orElse(""));
                }
            }

            document.add(table);
            document.close();
            return data;
        } catch (IOException | DocumentException e) {
            throw new DownloadException("Pdf file can't be generated!", e);
        }
    }


    private PdfPCell createHeader(String title) {
        PdfPCell header = new PdfPCell();
        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        header.setBorderWidth(1);
        header.setPhrase(new Phrase(title));
        return header;
    }
}
