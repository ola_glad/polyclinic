package com.geekhub.schedule.download;

import com.geekhub.schedule.ScheduleDto;

import java.io.ByteArrayOutputStream;
import java.util.List;

public interface ScheduleDownloader {
    ByteArrayOutputStream getDownloadFile(List<ScheduleDto> scheduleEntries) throws DownloadException;
}
