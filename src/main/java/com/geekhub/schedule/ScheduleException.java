package com.geekhub.schedule;

public class ScheduleException extends Exception {
    public ScheduleException(String message, Throwable cause) {
        super(message, cause);
    }
}
