package com.geekhub.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class ScheduleRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final RowMapper<Schedule> mapper = (rs, rowNum) -> {
        Schedule schedule = new Schedule();
        schedule.setDoctorId(rs.getInt("doctor_id"));
        schedule.setDate(rs.getDate("date").toLocalDate());
        schedule.setStartTime(rs.getTime("start_time").toLocalTime());
        schedule.setEndTime(rs.getTime("end_time").toLocalTime());
        return schedule;
    };

    @Autowired
    public ScheduleRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Schedule> getScheduleByDoctorsForPeriod(Set<Integer> doctorIds, LocalDate startDate, LocalDate endDate) {
        if (!doctorIds.isEmpty()) {
            String listIds = doctorIds.stream().map(Object::toString).collect(Collectors.joining(", "));
            String sql = "SELECT doctor_id, date, start_time, end_time" +
                    " FROM schedule " +
                    " WHERE date BETWEEN :startDate AND :endDate" +
                    " AND doctor_id in (" + listIds + ")" +
                    " ORDER BY doctor_id, date, start_time";
            MapSqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("startDate", Date.valueOf(startDate))
                    .addValue("endDate", Date.valueOf(endDate));
            return jdbcTemplate.query(sql, parameters, mapper);
        } else {
            return new ArrayList<>();
        }
    }

    public Optional<Schedule> getScheduleByDoctorOnDate(int doctorId, LocalDate date) {
        String sql = "SELECT doctor_id, date, start_time, end_time" +
                " FROM schedule " +
                " WHERE doctor_id = :doctorId " +
                " AND date = :date";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("doctorId", doctorId)
                .addValue("date", Date.valueOf(date));
        try {
            Schedule schedule = jdbcTemplate.queryForObject(sql, parameters, mapper);
            return Optional.of(schedule);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public boolean isExists(int doctorId, LocalDate date) {
        String sql = "SELECT EXISTS " +
                "(SELECT 1 FROM schedule WHERE doctor_id = :doctorId and date = :date) schedule_exists";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("doctorId", doctorId)
                .addValue("date", Date.valueOf(date));
        return jdbcTemplate.queryForObject(sql, parameters, boolean.class);
    }

    public void deleteSchedule(int doctorId, LocalDate date) {
        String sql = "DELETE FROM schedule WHERE doctor_id = :doctorId and date = :date";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("doctorId", doctorId)
                .addValue("date", Date.valueOf(date));
        jdbcTemplate.update(sql, parameters);
    }

    public void saveSchedule(Schedule schedule) {
        if (isExists(schedule.getDoctorId(), schedule.getDate())) {
            updateSchedule(schedule);
        } else {
            addSchedule(schedule);
        }
    }

    private void addSchedule(Schedule schedule) {
        String sql = "INSERT INTO schedule" +
                " (doctor_id, date, start_time, end_time)" +
                " VALUES ( :doctorId, :date, :startTime, :endTime)";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("doctorId", schedule.getDoctorId())
                .addValue("date", Date.valueOf(schedule.getDate()))
                .addValue("startTime", Time.valueOf(schedule.getStartTime()))
                .addValue("endTime", Time.valueOf(schedule.getEndTime()));
        jdbcTemplate.update(sql, parameters);
    }

    private void updateSchedule(Schedule schedule) {
        String sql = "UPDATE schedule SET " +
                " start_time = :startTime, " +
                " end_time = :endTime " +
                " WHERE doctor_id = :doctorId AND date = :date";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("doctorId", schedule.getDoctorId())
                .addValue("date", Date.valueOf(schedule.getDate()))
                .addValue("startTime", Time.valueOf(schedule.getStartTime()))
                .addValue("endTime", Time.valueOf(schedule.getEndTime()));
        jdbcTemplate.update(sql, parameters);
    }
}
