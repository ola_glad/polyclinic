package com.geekhub.pagination;

import java.util.List;

public class Page<T> {
    private List<T> data;
    private Pagination pagination;
    private int total;

    public Page(List<T> data, Pagination pagination, int total) {
        this.data = data;
        this.pagination = pagination;
        this.total = total;
    }

    public int getTotalPages() {
        return (int)Math.ceil((double) total / (double)pagination.getPageSize());
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
