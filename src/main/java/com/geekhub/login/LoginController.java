package com.geekhub.login;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/login")
public class LoginController {
    @GetMapping
    public String getLoginPage(
            @RequestParam(value="error", required = false, defaultValue = "false") boolean error,
            ModelMap model
    ) {
        model.addAttribute("error", error);
        return "login";
    }
}
