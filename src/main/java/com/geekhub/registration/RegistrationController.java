package com.geekhub.registration;

import com.geekhub.doctor.DoctorService;
import com.geekhub.patient.PatientRegistration;
import com.geekhub.patient.PatientService;
import com.geekhub.user.UserRegistration;
import com.geekhub.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    private final PatientService patientService;
    private final DoctorService doctorService;

    @Autowired
    public RegistrationController(PatientService patientService, DoctorService doctorService) {
        this.patientService = patientService;
        this.doctorService = doctorService;
    }

    @GetMapping
    public String getRegistrationPage(ModelMap model) {
        PatientRegistration registration = new PatientRegistration();
        registration.setRole(UserRole.PATIENT);
        model.addAttribute("registration", registration);
        return "registration";
    }

    @PostMapping
    public String registerPatient(
            @ModelAttribute("registration") @Valid PatientRegistration registration,
            BindingResult bindingResult,
            HttpServletRequest request
    ) throws ServletException {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        patientService.register(registration);
        request.login(registration.getLogin(), registration.getPassword());
        return "redirect:/";
    }
}
