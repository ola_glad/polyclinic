package com.geekhub.error;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorController {
    @ExceptionHandler(Throwable.class)
    public String exception(Throwable throwable, Model model) {
        model.addAttribute("errorMessage", throwable.getMessage());
        return "error";
    }
}
