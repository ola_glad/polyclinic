package com.geekhub.reception;

import java.time.LocalDateTime;

public class Reception {
    private int id;
    private int doctorId;
    private int patientId;
    private LocalDateTime date;
    private String medication;
    private ReceptionStatus status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public ReceptionStatus getStatus() {
        return status;
    }

    public void setStatus(ReceptionStatus status) {
        this.status = status;
    }

    public boolean isNewReception() {
        return this.id == 0;
    }
}
