package com.geekhub.reception;

import com.geekhub.user.User;
import com.geekhub.user.UserRepository;
import com.geekhub.user.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReceptionService {
    private final ReceptionRepository receptionRepository;
    private final UserRepository userRepository;
    private final UserUtils userUtils;

    @Autowired
    public ReceptionService(ReceptionRepository receptionRepository, UserRepository userRepository, UserUtils userUtils) {
        this.receptionRepository = receptionRepository;
        this.userRepository = userRepository;
        this.userUtils = userUtils;
    }

    public List<ReceptionDto> getTodayReceptionRequests(int userId) {
        return receptionsToDto(receptionRepository.getReceptionsByDateAndStatus(LocalDate.now(), userId, ReceptionStatus.REQUESTED));
    }

    public List<ReceptionDto> getReceptionRequestsByDate(int userId, LocalDate date) {
        return receptionsToDto(receptionRepository.getReceptionsByDateAndStatus(date, userId, ReceptionStatus.REQUESTED));
    }

    public List<ReceptionDto> getReceptions(int userId) {
        return receptionsToDto(receptionRepository.getReceptionsByUserId(userId));
    }

    public Reception request(ReceptionRequest request) {
        Reception reception = new Reception();
        reception.setDoctorId(request.getDoctorId());
        reception.setPatientId(request.getPatientId());
        reception.setDate(request.getDate().atTime(request.getTime()));
        reception.setStatus(ReceptionStatus.REQUESTED);
        return receptionRepository.saveReception(reception);
    }

    public Reception cancel(int id) throws ReceptionException {
        Reception reception = getReceptionById(id);
        reception.setStatus(ReceptionStatus.CANCELLED);
        return receptionRepository.saveReception(reception);
    }

    public ReceptionDto getReceptionDtoById(int id) throws ReceptionException {
        Reception reception = getReceptionById(id);
        Map<Integer, User> usersById = getUsersByReceptions(Collections.singletonList(reception));
        User doctor = usersById.get(reception.getDoctorId());
        User patient = usersById.get(reception.getPatientId());
        return convertToDto(reception, doctor, patient);
    }

    public Reception getReceptionById(int id) throws ReceptionException {
        Optional<Reception> receptionById = receptionRepository.getReceptionById(id);
        if (receptionById.isPresent()) {
            return receptionById.get();
        } else {
            throw new ReceptionException(String.format("Reception with id = %s not found", id));
        }
    }

    public Reception apply(ReceptionDto receptionDto) throws ReceptionException {
        Reception reception = getReceptionById(receptionDto.getId());
        reception.setMedication(receptionDto.getMedication());
        reception.setStatus(ReceptionStatus.APPLIED);
        return receptionRepository.saveReception(reception);
    }

    private Map<Integer, User> getUsersByReceptions(List<Reception> receptions) {
        List<Integer> userIds = receptions.stream()
                .map(reception -> Arrays.asList(reception.getDoctorId(), reception.getPatientId()))
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());

        return userRepository.getUserById(userIds).stream()
                .collect(Collectors.toMap(User::getId, Function.identity()));
    }

    private List<ReceptionDto> receptionsToDto(List<Reception> receptions) {
        Map<Integer, User> usersById = getUsersByReceptions(receptions);
        List<ReceptionDto> dtos = new ArrayList<>();
        for (Reception reception : receptions) {
            User doctor = usersById.get(reception.getDoctorId());
            User patient = usersById.get(reception.getPatientId());
            dtos.add(convertToDto(reception, doctor, patient));
        }
        return dtos;
    }

    private ReceptionDto convertToDto(Reception reception, User doctor, User patient)  {
        ReceptionDto dto = new ReceptionDto();
        dto.setId(reception.getId());
        dto.setPatient(userUtils.getFullName(patient.getFirstName(), patient.getLastName()));
        dto.setDoctor(userUtils.getFullName(doctor.getFirstName(), doctor.getLastName()));
        dto.setDate(reception.getDate());
        dto.setMedication(reception.getMedication());
        dto.setStatus(reception.getStatus());
        return dto;
    }
}
