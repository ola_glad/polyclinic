package com.geekhub.reception;

import com.geekhub.doctor.DoctorService;
import com.geekhub.user.CustomUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/reception")
public class ReceptionController {
    private final DoctorService doctorService;
    private final ReceptionService receptionService;

    @Autowired
    public ReceptionController(DoctorService doctorService, ReceptionService receptionService) {
        this.doctorService = doctorService;
        this.receptionService = receptionService;
    }

    @GetMapping
    @Secured({"ROLE_DOCTOR", "ROLE_ADMIN"})
    public String getReceptionList(ModelMap model, @AuthenticationPrincipal User user) throws ReceptionException {
        model.addAttribute("receptions", receptionService.getTodayReceptionRequests(((CustomUser) user).getId()));
        return "reception";
    }

    @GetMapping("/request")
    @Secured("ROLE_PATIENT")
    public String requestReception(ModelMap model, @AuthenticationPrincipal User user) {
        ReceptionRequest receptionRequest = new ReceptionRequest();
        receptionRequest.setPatientId(((CustomUser) user).getId());
        model.addAttribute("doctors", doctorService.getActiveDoctors());
        model.addAttribute("receptionRequest", receptionRequest);
        return "reception-request";
    }

    @PostMapping("/request")
    @Secured("ROLE_PATIENT")
    public String saveRequest(ReceptionRequest receptionRequest) {
        receptionService.request(receptionRequest);
        return "redirect:/";
    }

    @GetMapping("/requested/{doctorId}/{date}")
    @ResponseBody
    @Secured("ROLE_PATIENT")
    public List<ReceptionDto> getRequestedReceptions(
            @PathVariable int doctorId,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable LocalDate date
    ) {
        return receptionService.getReceptionRequestsByDate(doctorId, date);
    }

    @PostMapping("/cancel/{id}")
    @Secured("ROLE_PATIENT")
    public String cancelRequest(@PathVariable int id) throws ReceptionException {
        receptionService.cancel(id);
        return "redirect:/";
    }

    @GetMapping("/apply/{id}")
    @Secured({"ROLE_DOCTOR", "ROLE_ADMIN"})
    public String getReception(@PathVariable int id, ModelMap model) throws ReceptionException {
        model.addAttribute("reception", receptionService.getReceptionDtoById(id));
        return "reception-apply";
    }

    @PostMapping("/apply")
    @Secured({"ROLE_DOCTOR", "ROLE_ADMIN"})
    public String applyReception(ReceptionDto reception) throws ReceptionException {
        receptionService.apply(reception);
        return "redirect:/reception";
    }
}
