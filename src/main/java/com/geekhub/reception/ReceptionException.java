package com.geekhub.reception;

public class ReceptionException extends Exception {
    public ReceptionException(String message) {
        super(message);
    }

    public ReceptionException(String message, Throwable cause) {
        super(message, cause);
    }
}
