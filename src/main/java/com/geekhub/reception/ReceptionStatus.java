package com.geekhub.reception;

public enum ReceptionStatus {
    REQUESTED, CANCELLED, APPLIED
}
