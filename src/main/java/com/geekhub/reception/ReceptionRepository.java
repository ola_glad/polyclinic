package com.geekhub.reception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class ReceptionRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final RowMapper<Reception> mapper = (rs, rowNum) -> {
        Reception reception = new Reception();
        reception.setId(rs.getInt("id"));
        reception.setDoctorId(rs.getInt("doctor_id"));
        reception.setPatientId(rs.getInt("patient_id"));
        reception.setDate(rs.getTimestamp("date").toLocalDateTime());
        reception.setMedication(rs.getString("medication"));
        reception.setStatus(ReceptionStatus.valueOf(rs.getString("status")));
        return reception;
    };

    @Autowired
    public ReceptionRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<Reception> getReceptionById(int id) {
        String sql = "SELECT " +
                " id, doctor_id, patient_id, date, medication, status" +
                " FROM receptions WHERE id = " + id;
        try {
            Reception reception = jdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), mapper);
            return Optional.of(reception);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public Reception saveReception(Reception reception) {
        if (reception.isNewReception()) {
            return addReception(reception);
        } else {
            return updateReception(reception);
        }
    }

    private Reception addReception(Reception reception) {
        String sql = "INSERT INTO receptions" +
                " (doctor_id, patient_id, date, status)" +
                " VALUES ( :doctorId, :patientId, :date, :status)";
        KeyHolder holder = new GeneratedKeyHolder();
        MapSqlParameterSource mapSource = new MapSqlParameterSource()
                .addValue("doctorId", reception.getDoctorId())
                .addValue("patientId", reception.getPatientId())
                .addValue("date", Timestamp.valueOf(reception.getDate()))
                .addValue("status", reception.getStatus().toString());
        jdbcTemplate.update(sql, mapSource, holder, new String[] {"id"});

        Number key = holder.getKey();
        if (key != null) {
            reception.setId(key.intValue());
        }

        return reception;
    }

    private Reception updateReception(Reception reception) {
        String sql = "UPDATE receptions SET " +
                " doctor_id = :doctorId, " +
                " patient_id = :patientId, " +
                " date = :date, " +
                " medication = :medication, " +
                " status = :status " +
                " WHERE id = :id";
        MapSqlParameterSource mapSource = new MapSqlParameterSource()
                .addValue("doctorId", reception.getDoctorId())
                .addValue("patientId", reception.getPatientId())
                .addValue("date", Timestamp.valueOf(reception.getDate()))
                .addValue("medication", reception.getMedication())
                .addValue("status", reception.getStatus().toString())
                .addValue("id", reception.getId());
        jdbcTemplate.update(sql, mapSource);
        return reception;
    }

    public List<Reception> getReceptionsByDateAndStatus(LocalDate date, int userId, ReceptionStatus status) {
        LocalDate endDate = date.plusDays(1);
        String sql = "SELECT" +
                " id, doctor_id, patient_id, date, medication, status" +
                " FROM receptions" +
                " WHERE date BETWEEN :startDate AND :endDate" +
                " AND (doctor_id = :userId OR patient_id = :userId)" +
                " AND status = :status" +
                " ORDER BY date";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("startDate", Date.valueOf(date))
                .addValue("endDate", Date.valueOf(endDate))
                .addValue("status", status.toString())
                .addValue("userId", userId);
        return jdbcTemplate.query(sql, parameters, mapper);
    }

    public List<Reception> getReceptionsByUserId(int userId) {
        String sql = "SELECT" +
                " id, doctor_id, patient_id, date, medication, status" +
                " FROM receptions" +
                " WHERE doctor_id = " + userId + " OR patient_id = " + userId +
                " ORDER BY date DESC";
        return jdbcTemplate.query(sql, mapper);
    }
}
