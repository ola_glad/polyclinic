package com.geekhub.reception;

import java.time.LocalDateTime;

public class ReceptionDto {
    private int id;
    private String doctor;
    private String patient;
    private LocalDateTime date;
    private String medication;
    private ReceptionStatus status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public ReceptionStatus getStatus() {
        return status;
    }

    public void setStatus(ReceptionStatus status) {
        this.status = status;
    }
}
