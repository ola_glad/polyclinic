package com.geekhub.admin;

import com.geekhub.pagination.Page;
import com.geekhub.pagination.Pagination;
import com.geekhub.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AdminUserService {
    private final UserRepository userRepository;
    private final UserUtils userUtils;
    private final UserEditConverter userEditConverter;

    @Autowired
    public AdminUserService(UserRepository userRepository, UserUtils userUtils, UserEditConverter userEditConverter) {
        this.userRepository = userRepository;
        this.userUtils = userUtils;
        this.userEditConverter = userEditConverter;
    }

    public Page<UserDto> getUsersPage(int page, int pageSize) {
        Pagination pagination = new Pagination(page, pageSize);
        int offset = pagination.getPageOffset();
        int total = userRepository.getTotalCount();
        List<UserDto> users = userRepository.getUsers(offset, pageSize).stream()
                .map(this::convertToDto).collect(Collectors.toList());
        return new Page<>(users, pagination, total);
    }

    public UserEdit getEditUserPage(int id) throws AdminException {
        Optional<User> userById = userRepository.getUserById(id);
        if (userById.isPresent()) {
            User user = userById.get();
            return userEditConverter.convert(user);
        } else {
            throw new AdminException(String.format("User with id %d not found", id));
        }
    }

    public void inactivate(int id) {
        userRepository.changeUserActive(false, id);
    }

    private UserDto convertToDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setFullName(userUtils.getFullName(user.getFirstName(), user.getLastName()));
        userDto.setEmail(user.getEmail());
        userDto.setPhone(user.getPhone());
        userDto.setActive(user.isActive());
        userDto.setRole(user.getRole());
        return userDto;
    }
}
