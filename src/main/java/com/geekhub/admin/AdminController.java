package com.geekhub.admin;

import com.geekhub.doctor.DoctorService;
import com.geekhub.pagination.Page;
import com.geekhub.user.UserDto;
import com.geekhub.user.UserEdit;
import com.geekhub.user.UserRegistration;
import com.geekhub.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
@Secured("ROLE_ADMIN")
public class AdminController {
    private final AdminUserService adminUserService;
    private final DoctorService doctorService;

    @Autowired
    public AdminController(AdminUserService adminUserService, DoctorService doctorService) {
        this.adminUserService = adminUserService;
        this.doctorService = doctorService;
    }

    @GetMapping
    public String getAdminPage() {
        return "admin";
    }

    @GetMapping("/users")
    @ResponseBody
    public Page<UserDto> getUsers(
        @RequestParam(value = "page", required = false, defaultValue = "${request.defaultPage}") int page,
        @RequestParam(value = "pageSize", required = false, defaultValue = "${request.defaultPageSize}") int pageSize) {
        return adminUserService.getUsersPage(page, pageSize);
    }

    @GetMapping("/add/{role}")
    public String getAddPage(@PathVariable UserRole role, ModelMap model) {
        UserRegistration registration = new UserRegistration();
        registration.setRole(role);
        model.addAttribute("registration", registration);
        return "registration";
    }

    @PostMapping("/add")
    public String registerDoctor(@ModelAttribute @Valid UserRegistration registration, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        doctorService.add(registration);
        return "redirect:/admin";
    }

    @GetMapping("/edit/{id}")
    public String getEditPage(@PathVariable int id, ModelMap model) throws AdminException {
        UserEdit edit = adminUserService.getEditUserPage(id);
        model.addAttribute("user", edit);
        return "edit-user";
    }

    @PostMapping("/inactivate/{id}")
    public String inactivateDoctor(@PathVariable int id) {
        adminUserService.inactivate(id);
        return "redirect:/admin";
    }
}
