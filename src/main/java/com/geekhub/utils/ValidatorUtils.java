package com.geekhub.utils;

import javax.validation.ConstraintValidatorContext;

public class ValidatorUtils {
    public static void setErrorOnField(String fieldName, String message, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message)
                .addPropertyNode(fieldName)
                .addConstraintViolation();
    }
}
