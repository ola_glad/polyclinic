package com.geekhub.user;

import com.geekhub.user.validator.UserPasswordConstraint;

@UserPasswordConstraint
public class UserChangePassword extends UserPassword {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
