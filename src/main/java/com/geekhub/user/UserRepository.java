package com.geekhub.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final RowMapper<User> mapper = (rs, rowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        user.setFirstName(rs.getString("first_name"));
        user.setLastName(rs.getString("last_name"));
        user.setEmail(rs.getString("email"));
        user.setPhone(rs.getString("phone"));
        user.setRole(UserRole.valueOf(rs.getString("role")));
        user.setActive(rs.getBoolean("active"));
        user.setPassword(rs.getString("password"));
        return user;
    };

    @Autowired
    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<User> getUserById(int id) {
        String sql = "SELECT " +
                " id, first_name, last_name, login, password, email, phone, active, role" +
                " FROM users WHERE id = " + id;
        try {
            User user = jdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), mapper);
            return Optional.of(user);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public List<User> getUserById(List<Integer> ids) {
        if (!ids.isEmpty()) {
            String listIds = ids.stream().map(Object::toString).collect(Collectors.joining(", "));
            String sql = "SELECT " +
                    " id, first_name, last_name, login, password, email, phone, active, role" +
                    " FROM users " +
                    " WHERE id in (" + listIds + ") " +
                    " ORDER BY first_name, last_name";
            return jdbcTemplate.query(sql, mapper);
        } else {
            return new ArrayList<>();
        }
    }

    public Optional<User> getUserByUsername(String username) {
        String sql = "SELECT " +
                " id, first_name, last_name, login, password, email, phone, active, role" +
                " FROM users WHERE login = :username";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("username", username);
        try {
            User user = jdbcTemplate.queryForObject(sql, parameters, mapper);
            return Optional.of(user);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public boolean isExists(int id) {
        String sql = "SELECT EXISTS " +
                "(SELECT 1 FROM users WHERE id = " + id + ") users_exists";
        return jdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), boolean.class);
    }

    public boolean isExists(String login) {
        String sql = "SELECT EXISTS " +
                "(SELECT 1 FROM users WHERE login = :login) users_exists";
        MapSqlParameterSource parameters = new MapSqlParameterSource().addValue("login", login);
        return jdbcTemplate.queryForObject(sql, parameters, boolean.class);
    }

    public User saveUser(User user) {
        if (user.isNewUser()) {
            return addUser(user);
        } else {
            return updateUser(user);
        }
    }

    private User addUser(User user) {
        String sql = "INSERT INTO users" +
                " (first_name, last_name, login, email, phone, role, active, password)" +
                " VALUES ( :firstName, :lastName, :login, :email, :phone, :role, :active, :password)";
        KeyHolder holder = new GeneratedKeyHolder();
        MapSqlParameterSource mapSource = new MapSqlParameterSource()
                .addValue("firstName", user.getFirstName())
                .addValue("lastName", user.getLastName())
                .addValue("login", user.getLogin())
                .addValue("email", user.getEmail())
                .addValue("phone", user.getPhone())
                .addValue("role", user.getRole().toString())
                .addValue("active", user.isActive())
                .addValue("password", user.getPassword());
        jdbcTemplate.update(sql, mapSource, holder, new String[] {"id"});

        Number key = holder.getKey();
        if (key != null) {
            user.setId(key.intValue());
        }

        return user;
    }

    private User updateUser(User user) {
        String sql = "UPDATE users SET " +
                " first_name = :firstName, " +
                " last_name = :lastName, " +
                " login = :login, " +
                " email = :email, " +
                " phone = :phone, " +
                " role = :role, " +
                " active = :active, " +
                " password = :password " +
                " WHERE id = :id";
        MapSqlParameterSource mapSource = new MapSqlParameterSource()
                .addValue("firstName", user.getFirstName())
                .addValue("lastName", user.getLastName())
                .addValue("login", user.getLogin())
                .addValue("email", user.getEmail())
                .addValue("phone", user.getPhone())
                .addValue("role", user.getRole().toString())
                .addValue("active", user.isActive())
                .addValue("password", user.getPassword())
                .addValue("id", user.getId());
        jdbcTemplate.update(sql, mapSource);
        return user;
    }

    public void changeUserActive(boolean active, int id) {
        String sql = "UPDATE users SET active = :active WHERE id = :id";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("active", active)
                .addValue("id", id);
        jdbcTemplate.update(sql, parameters);
    }

    public int getTotalCount() {
        String sql = "SELECT count(*) AS total FROM users ";
        return jdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), int.class);
    }

    public List<User> getUsers(int offset, int limit) {
        String sql = "SELECT " +
                " id, first_name, last_name, login, password, email, phone, active, role" +
                " FROM users ORDER BY active DESC, role, first_name, last_name" +
                (limit > 0 ? " LIMIT " + limit + " OFFSET " + offset : "");
        return jdbcTemplate.query(sql, mapper);
    }

    public List<User> getActiveUsersByRole(List<UserRole> roles) {
        if (roles.isEmpty()) {
            return new ArrayList<>();
        }
        String rolesStr = roles.stream()
                .map(UserRole::toString)
                .map(role -> "'" + role + "'")
                .collect(Collectors.joining(", "));
        String sql = "SELECT " +
                " id, first_name, last_name, login, password, email, phone, active, role" +
                " FROM users WHERE active = TRUE and role in (" + rolesStr + ")" +
                " ORDER BY first_name, last_name";
        return jdbcTemplate.query(sql, mapper);
    }
}
