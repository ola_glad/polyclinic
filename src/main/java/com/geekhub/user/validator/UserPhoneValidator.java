package com.geekhub.user.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserPhoneValidator implements ConstraintValidator<UserPhoneConstraint, String> {

    @Override
    public void initialize(UserPhoneConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String phone, ConstraintValidatorContext context) {
        return phone != null && (phone.isEmpty() || phone.matches("^(8|\\+38|0038)?0(9\\d|39|50|63|66|67|68|73)\\d{7}"));
    }
}
