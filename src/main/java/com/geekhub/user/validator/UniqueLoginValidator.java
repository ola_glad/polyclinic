package com.geekhub.user.validator;

import com.geekhub.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueLoginValidator implements ConstraintValidator<UniqueLoginConstraint, String>  {
    private UserRepository userRepository;

    @Autowired
    public UniqueLoginValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void initialize(UniqueLoginConstraint constraint) {
    }

    @Override
    public boolean isValid(String login, ConstraintValidatorContext context) {
        return login != null && !userRepository.isExists(login);
    }
}
