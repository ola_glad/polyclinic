package com.geekhub.user.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueLoginValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueLoginConstraint {
    String message() default "{user.exists}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
