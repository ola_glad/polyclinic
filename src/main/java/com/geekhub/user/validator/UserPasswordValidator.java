package com.geekhub.user.validator;

import com.geekhub.user.UserPassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.geekhub.utils.ValidatorUtils.setErrorOnField;

public class UserPasswordValidator implements ConstraintValidator<UserPasswordConstraint, UserPassword> {
    @Override
    public void initialize(UserPasswordConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(UserPassword value, ConstraintValidatorContext context) {
        String password = value.getPassword();
        String confirmPassword = value.getConfirmPassword();

        if (password == null || password.isEmpty()) {
            setErrorOnField("password", "{password.empty}", context);
            return false;
        }

        if (confirmPassword == null || confirmPassword.isEmpty()) {
            setErrorOnField("confirmPassword", "{confirmPassword.empty}", context);
            return false;
        }

        if (!password.equals(confirmPassword)) {
            setErrorOnField("password", "{password.difference}", context);
            return false;
        }

        return true;
    }
}
