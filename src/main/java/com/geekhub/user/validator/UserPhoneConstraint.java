package com.geekhub.user.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UserPhoneValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserPhoneConstraint {
    String message() default "{user.phone}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
