package com.geekhub.user;

import org.springframework.stereotype.Component;

@Component
public class UserUtils {
    public String getFullName(String firstName, String lastName) {
        return String.format("%s %s", firstName, lastName);
    }
}
