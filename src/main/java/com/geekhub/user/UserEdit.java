package com.geekhub.user;

import com.geekhub.user.validator.UserEmailConstraint;
import com.geekhub.user.validator.UserPhoneConstraint;

import javax.validation.constraints.Size;

public class UserEdit {
    private int id;
    @Size(max = 60, message = "{size.less}")

    private String firstName;

    @Size(max = 60, message = "{size.less}")
    private String lastName;

    @UserEmailConstraint
    private String email;

    @UserPhoneConstraint
    private String phone;

    private UserRole role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
