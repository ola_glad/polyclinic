package com.geekhub.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userByUsername = userRepository.getUserByUsername(username);
        if (userByUsername.isPresent()) {
            User user = userByUsername.get();
            if (user.isActive()) {
                List<GrantedAuthority> roles = Collections
                        .singletonList(new SimpleGrantedAuthority("ROLE_" + user.getRole().toString()));

                CustomUser customUser = new CustomUser(user.getLogin(), user.getPassword(), roles);
                customUser.setId(user.getId());
                return customUser;
            } else {
                throw new UsernameNotFoundException("User with username " + username + " is inactive");
            }
        } else {
            throw new UsernameNotFoundException("User with username " + username + " not found");
        }
    }
}
