package com.geekhub.user;

public enum UserRole {
    ADMIN, DOCTOR, PATIENT
}
