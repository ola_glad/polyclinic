package com.geekhub.user;

import org.springframework.stereotype.Component;

@Component
public class UserEditConverter {
    public UserEdit convert(User user) {
        return convert(user, new UserEdit());
    }

    public UserEdit convert(User user, UserEdit edit) {
        edit.setId(user.getId());
        edit.setFirstName(user.getFirstName());
        edit.setLastName(user.getLastName());
        edit.setEmail(user.getEmail());
        edit.setPhone(user.getPhone());
        edit.setRole(user.getRole());
        return edit;
    }
}
