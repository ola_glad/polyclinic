package com.geekhub.statistic;

import com.geekhub.reception.ReceptionStatus;
import com.geekhub.report.DoctorReport;
import com.geekhub.report.ReceptionReport;
import com.geekhub.report.ReportService;
import com.geekhub.user.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatisticService {
    private final ReportService reportService;
    private final UserUtils userUtils;

    @Autowired
    public StatisticService(ReportService reportService, UserUtils userUtils) {
        this.reportService = reportService;
        this.userUtils = userUtils;
    }

    public List<ReceptionReportDto> getReceptionReport(ReportParams par) {
        return reportService.getReceptionReport(par.getDoctorId(), par.getStartDate(), par.getEndDate()).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    public List<DoctorReportDto> getDoctorReport(ReportParams par) {
        return reportService.getDoctorReport(par.getStartDate(), par.getEndDate(), ReceptionStatus.APPLIED.toString()).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private ReceptionReportDto convertToDto(ReceptionReport report) {
        ReceptionReportDto dto = new ReceptionReportDto();
        dto.setDoctor(userUtils.getFullName(report.getDoctorFirstName(), report.getDoctorLastName()));
        dto.setPatient(userUtils.getFullName(report.getPatientFirstName(), report.getPatientLastName()));
        dto.setDate(report.getDate());
        dto.setMedication(report.getMedication());
        dto.setStatus(report.getStatus());
        dto.setCountReceptions(report.getCountReceptions());
        dto.setCountByStatus(report.getCountByStatus());
        return dto;
    }

    private DoctorReportDto convertToDto(DoctorReport report) {
        DoctorReportDto dto = new DoctorReportDto();
        dto.setDoctor(userUtils.getFullName(report.getFirstName(), report.getLastName()));
        dto.setTotalCount(report.getTotalCount());
        dto.setStatusCount(report.getStatusCount());
        dto.setMaxCount(report.getMaxCount());
        dto.setMinCount(report.getMinCount());
        return dto;
    }
}
