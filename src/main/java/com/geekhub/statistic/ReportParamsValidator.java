package com.geekhub.statistic;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

import static com.geekhub.utils.ValidatorUtils.setErrorOnField;

public class ReportParamsValidator implements ConstraintValidator<ReportParamsConstraint, ReportParams> {
    @Override
    public void initialize(ReportParamsConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(ReportParams value, ConstraintValidatorContext context) {
        LocalDate startDate = value.getStartDate();
        LocalDate endDate = value.getEndDate();

        if (startDate == null) {
            setErrorOnField("startDate", "{notNull}", context);
            return false;
        }

        if (endDate == null) {
            setErrorOnField("endDate", "{notNull}", context);
            return false;
        }

        if (startDate.isAfter(endDate)) {
            setErrorOnField("startDate", "{date.order}", context);
            return false;
        }

        return true;
    }
}
