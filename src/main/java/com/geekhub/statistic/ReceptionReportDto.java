package com.geekhub.statistic;

import java.time.LocalDateTime;

public class ReceptionReportDto {
    private String doctor;
    private String patient;
    private LocalDateTime date;
    private String medication;
    private String status;
    private int countReceptions;
    private int countByStatus;

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getPatient() {
        return patient;
    }

    public void setPatient(String patient) {
        this.patient = patient;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCountReceptions() {
        return countReceptions;
    }

    public void setCountReceptions(int countReceptions) {
        this.countReceptions = countReceptions;
    }

    public int getCountByStatus() {
        return countByStatus;
    }

    public void setCountByStatus(int countByStatus) {
        this.countByStatus = countByStatus;
    }
}
