package com.geekhub.statistic;

import com.geekhub.doctor.DoctorDto;
import com.geekhub.doctor.DoctorService;
import com.geekhub.user.CustomUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/statistic")
public class StatisticController {
    private final StatisticService statisticService;
    private final DoctorService doctorService;

    @Autowired
    public StatisticController(StatisticService statisticService, DoctorService doctorService) {
        this.statisticService = statisticService;
        this.doctorService = doctorService;
    }

    @GetMapping("/receptions")
    @Secured({"ROLE_DOCTOR", "ROLE_ADMIN"})
    public String getReceptionsPage(ModelMap model, @AuthenticationPrincipal User user) {
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        boolean isAdmin = authorities.contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
        int userId = ((CustomUser) user).getId();
        List<DoctorDto> doctors = doctorService.getActiveDoctors();
        LocalDate endDate = LocalDate.now().minusDays(1);
        LocalDate startDate = endDate.minusMonths(1);

        model.addAttribute("doctors", isAdmin ? doctors : doctors.stream()
                .filter(doctor -> doctor.getId() == userId)
                .collect(Collectors.toList()));
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        return "statistic-receptions";
    }

    @PostMapping("/receptions")
    @Secured({"ROLE_DOCTOR", "ROLE_ADMIN"})
    public @ResponseBody List<ReceptionReportDto> getReceptionReport(@Valid ReportParams reportParams) {
        return statisticService.getReceptionReport(reportParams);
    }

    @GetMapping("/doctors")
    @Secured("ROLE_ADMIN")
    public String getDoctorsPage(ModelMap model) {
        LocalDate endDate = LocalDate.now().minusDays(1);
        LocalDate startDate = endDate.minusMonths(1);

        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        return "statistic-doctors";
    }

    @PostMapping("/doctors")
    @Secured("ROLE_ADMIN")
    public @ResponseBody List<DoctorReportDto> getDoctorReport(@Valid ReportParams reportParams) {
        return statisticService.getDoctorReport(reportParams);
    }

    @ResponseStatus(value= HttpStatus.BAD_REQUEST, reason = "bad request")
    @ExceptionHandler
    public void handleException(BindException ex) {

    }
}
