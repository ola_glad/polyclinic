package com.geekhub.account;

import java.time.LocalDate;

public class PatientAccountDto extends AccountDto {
    private String address;
    private LocalDate birthday;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
}
