package com.geekhub.account;

import com.geekhub.doctor.DoctorException;
import com.geekhub.doctor.DoctorService;
import com.geekhub.patient.PatientEdit;
import com.geekhub.patient.PatientException;
import com.geekhub.patient.PatientService;
import com.geekhub.reception.ReceptionService;
import com.geekhub.user.CustomUser;
import com.geekhub.user.UserChangePassword;
import com.geekhub.user.UserEdit;
import com.geekhub.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/account")
public class AccountController {
    private final AccountService accountService;
    private final ReceptionService receptionService;
    private final DoctorService doctorService;
    private final PatientService patientService;

    @Autowired
    public AccountController(AccountService accountService, ReceptionService receptionService, DoctorService doctorService, PatientService patientService) {
        this.accountService = accountService;
        this.receptionService = receptionService;
        this.doctorService = doctorService;
        this.patientService = patientService;
    }

    @GetMapping
    public String myAccount(ModelMap model, @AuthenticationPrincipal User user) throws AccountException {
        int userId = ((CustomUser) user).getId();
        AccountDto account = accountService.getAccountById(userId);
        model.addAttribute("isPatient", UserRole.PATIENT.equals(account.getRole()));
        model.addAttribute("user", account);
        model.addAttribute("receptions", receptionService.getReceptions(userId));
        return "my-account";
    }

    @GetMapping("/edit")
    public String getEditPage(ModelMap model, @AuthenticationPrincipal User user) throws AccountException {
        int userId = ((CustomUser) user).getId();
        UserEdit edit = accountService.getEditAccountPage(userId);
        model.addAttribute("user", edit);
        return "edit-user";
    }

    @PostMapping("/edit/user")
    public String editUserData(@ModelAttribute("user") @Valid UserEdit user, BindingResult bindingResult) throws DoctorException {
        if (bindingResult.hasErrors()) {
            return "edit-user";
        }
        doctorService.edit(user);
        return "redirect:/";
    }

    @PostMapping("/edit/patient")
    public String editPatientData(@ModelAttribute("user") @Valid PatientEdit user, BindingResult bindingResult) throws PatientException {
        if (bindingResult.hasErrors()) {
            return "edit-user";
        }
        patientService.edit(user);
        return "redirect:/";
    }

    @GetMapping("/change-password")
    public String getChangePasswordPage(ModelMap model, @AuthenticationPrincipal User user) {
        UserChangePassword changePassword = new UserChangePassword();
        changePassword.setId(((CustomUser) user).getId());
        model.addAttribute("changePassword", changePassword);
        return "change-password";
    }

    @PostMapping("/change-password")
    public String changePassword(@ModelAttribute("changePassword") @Valid UserChangePassword changePassword, BindingResult bindingResult) throws AccountException {
        if (bindingResult.hasErrors()) {
            return "change-password";
        }
        accountService.changePassword(changePassword);
        return "redirect:/account";
    }
}
