package com.geekhub.account;

import com.geekhub.patient.Patient;
import com.geekhub.patient.PatientEdit;
import com.geekhub.patient.PatientService;
import com.geekhub.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {
    private final UserRepository userRepository;
    private final PatientService patientService;
    private final PasswordEncoder passwordEncoder;
    private final UserEditConverter userEditConverter;

    @Autowired
    public AccountService(UserRepository userRepository, PatientService patientService, PasswordEncoder passwordEncoder, UserEditConverter userEditConverter) {
        this.userRepository = userRepository;
        this.patientService = patientService;
        this.passwordEncoder = passwordEncoder;
        this.userEditConverter = userEditConverter;
    }

    public AccountDto getAccountById(int id) throws AccountException {
        Optional<User> userById = userRepository.getUserById(id);
        if (userById.isPresent()) {
            User user = userById.get();
            if (UserRole.PATIENT.equals(user.getRole())) {
                Patient patient = patientService.getPatientData(user);
                PatientAccountDto patientDto = new PatientAccountDto();
                patientDto.setBirthday(patient.getBirthday());
                patientDto.setAddress(patient.getAddress());
                return fillAccountDto(patient, patientDto);
            } else {
                return fillAccountDto(user, new AccountDto());
            }
        } else {
            throw new AccountException(String.format("User with id %d not found", id));
        }
    }

    public UserEdit getEditAccountPage(int id) throws AccountException {
        Optional<User> userById = userRepository.getUserById(id);
        if (userById.isPresent()) {
            User user = userById.get();
            if (UserRole.PATIENT.equals(user.getRole())) {
                Patient patient = patientService.getPatientData(user);
                PatientEdit patientEdit = new PatientEdit();
                patientEdit.setBirthday(patient.getBirthday());
                patientEdit.setAddress(patient.getAddress());
                return userEditConverter.convert(patient, patientEdit);
            } else {
                return userEditConverter.convert(user);
            }
        } else {
            throw new AccountException(String.format("User with id %d not found", id));
        }
    }

    public void changePassword(UserChangePassword changePassword) throws AccountException {
        Optional<User> userById = userRepository.getUserById(changePassword.getId());
        if (userById.isPresent()) {
            User user = userById.get();
            user.setPassword(passwordEncoder.encode(changePassword.getPassword()));
            userRepository.saveUser(user);
        } else {
            throw new AccountException(String.format("User with id %d not found", changePassword.getId()));
        }
    }

    private AccountDto fillAccountDto(User user, AccountDto dto) {
        dto.setId(user.getId());
        dto.setLogin(user.getLogin());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());
        dto.setPhone(user.getPhone());
        dto.setRole(user.getRole());
        return dto;
    }
}
