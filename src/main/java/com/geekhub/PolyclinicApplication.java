package com.geekhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.geekhub")
public class PolyclinicApplication {
    public static void main(String[] args) {
        SpringApplication.run(PolyclinicApplication.class, args);
    }
}
