package com.geekhub.doctor;

public class DoctorException extends Exception {
    public DoctorException(String message, Throwable cause) {
        super(message, cause);
    }

    public DoctorException(String message) {
        super(message);
    }
}
