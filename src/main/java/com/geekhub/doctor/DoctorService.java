package com.geekhub.doctor;

import com.geekhub.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserUtils userUtils;

    @Autowired
    public DoctorService(UserRepository userRepository, PasswordEncoder passwordEncoder, UserUtils userUtils) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userUtils = userUtils;
    }

    public void add(UserRegistration userRegistration) {
        User user = new User();
        user.setLogin(userRegistration.getLogin());
        user.setFirstName(userRegistration.getFirstName());
        user.setLastName(userRegistration.getLastName());
        user.setEmail(userRegistration.getEmail());
        user.setPhone(userRegistration.getPhone());
        user.setRole(userRegistration.getRole());
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(userRegistration.getPassword()));
        userRepository.saveUser(user);
    }

    public void edit(UserEdit userEdit) throws DoctorException {
        Optional<User> userById = userRepository.getUserById(userEdit.getId());
        if (userById.isPresent()) {
            User user = userById.get();
            user.setFirstName(userEdit.getFirstName());
            user.setLastName(userEdit.getLastName());
            user.setEmail(userEdit.getEmail());
            user.setPhone(userEdit.getPhone());
            userRepository.saveUser(user);
        } else {
            throw new DoctorException(
                String.format("Doctor %s %s with id %d not found",
                        userEdit.getFirstName(),
                        userEdit.getLastName(),
                        userEdit.getId()));
        }
    }

    public List<DoctorDto> getActiveDoctors() {
        return userRepository.getActiveUsersByRole(Arrays.asList(UserRole.DOCTOR, UserRole.ADMIN)).stream()
                .map(this::convertDoctorToDto).collect(Collectors.toList());
    }

    public DoctorDto getDoctorById(int id) throws DoctorException {
        Optional<User> doctor = userRepository.getUserById(id);
        if (doctor.isPresent()) {
            return convertDoctorToDto(doctor.get());
        } else {
            throw new DoctorException(String.format("Doctor with id = %s not found", id));
        }
    }

    private DoctorDto convertDoctorToDto(User doctor) {
        DoctorDto doctorDto = new DoctorDto();
        doctorDto.setId(doctor.getId());
        doctorDto.setFullName(userUtils.getFullName(doctor.getFirstName(), doctor.getLastName()));
        return doctorDto;
    }
}
