package com.geekhub.patient;

import com.geekhub.user.User;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class Patient extends User {
    private String address;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    public Patient() {}

    public Patient(User user) {
        setId(user.getId());
        setLogin(user.getLogin());
        setFirstName(user.getFirstName());
        setLastName(user.getLastName());
        setEmail(user.getEmail());
        setPhone(user.getPhone());
        setPassword(user.getPassword());
        setRole(user.getRole());
        setActive(user.isActive());
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
}
