package com.geekhub.patient;

import com.geekhub.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;


@Repository
public class PatientRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public PatientRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Patient addPatient(Patient patient) {
        String sql = "INSERT INTO patients" +
                " (user_id, birthday, address)" +
                " VALUES (:userId, :birthday, :address)";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("userId", patient.getId())
                .addValue("birthday", Date.valueOf(patient.getBirthday()))
                .addValue("address", patient.getAddress());
        jdbcTemplate.update(sql, parameters);
        return patient;
    }

    public Patient updatePatient(Patient patient) {
        String sql = "UPDATE patients" +
                " SET birthday = :birthday, " +
                " address = :address" +
                " WHERE user_id = :userId";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("userId", patient.getId())
                .addValue("birthday", Date.valueOf(patient.getBirthday()))
                .addValue("address", patient.getAddress());
        jdbcTemplate.update(sql, parameters);
        return patient;
    }

    public Patient getPatient(User user) {
        String sql = "SELECT birthday, address FROM patients WHERE user_id = " + user.getId();
        try {
            return jdbcTemplate.queryForObject(sql, new MapSqlParameterSource(), (rs, rowNum) -> {
                Patient patient = new Patient(user);
                patient.setBirthday(rs.getDate("birthday").toLocalDate());
                patient.setAddress(rs.getString("address"));
                return patient;
            });
        } catch (EmptyResultDataAccessException e) {
            return new Patient(user);
        }
    }
}
