package com.geekhub.patient;

import com.geekhub.user.User;
import com.geekhub.user.UserRepository;
import com.geekhub.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PatientService {
    private final UserRepository userRepository;
    private final PatientRepository patientRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public PatientService(UserRepository userRepository, PatientRepository patientRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.patientRepository = patientRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public void register(PatientRegistration patientRegistration) {
        Patient patient = new Patient();
        patient.setLogin(patientRegistration.getLogin());
        patient.setFirstName(patientRegistration.getFirstName());
        patient.setLastName(patientRegistration.getLastName());
        patient.setEmail(patientRegistration.getEmail());
        patient.setPhone(patientRegistration.getPhone());
        patient.setRole(UserRole.PATIENT);
        patient.setActive(true);
        patient.setPassword(passwordEncoder.encode(patientRegistration.getPassword()));
        patient.setBirthday(patientRegistration.getBirthday());
        patient.setAddress(patientRegistration.getAddress());
        userRepository.saveUser(patient);
        patientRepository.addPatient(patient);
    }

    public Patient getPatientData(User user) {
        return patientRepository.getPatient(user);
    }

    @Transactional
    public void edit(PatientEdit patientEdit) throws PatientException {
        Optional<User> userById = userRepository.getUserById(patientEdit.getId());
        if (userById.isPresent()) {
            User user = userById.get();
            Patient patient = new Patient(user);
            patient.setFirstName(patientEdit.getFirstName());
            patient.setLastName(patientEdit.getLastName());
            patient.setEmail(patientEdit.getEmail());
            patient.setPhone(patientEdit.getPhone());
            patient.setBirthday(patientEdit.getBirthday());
            patient.setAddress(patientEdit.getAddress());
            userRepository.saveUser(patient);
            patientRepository.updatePatient(patient);
        } else {
            throw new PatientException(
                    String.format("Patient %s %s with id %d not found",
                            patientEdit.getFirstName(),
                            patientEdit.getLastName(),
                            patientEdit.getId()));
        }
    }
}
