package db.migration;

import com.geekhub.user.UserRole;
import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class V20180329220000__insert_user_admin implements SpringJdbcMigration {
    @Override
    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        String sql = "INSERT INTO users (login, first_name, last_name, email, role, active, password)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql,
                "superadmin",
                "admin",
                "super",
                "admin@gmail.com",
                UserRole.ADMIN.toString(),
                true,
                (new BCryptPasswordEncoder()).encode("1")
        );
    }
}
